//
//  ChartsController.swift
//  Plant Tracker
//
//  Created by Matteo Cioppa on 04/04/2020.
//  Copyright © 2020 Matteo Cioppa. All rights reserved.
//

import UIKit
import CareKit

class ChartsController: OCKDailyPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func dailyPageViewController(_ dailyPageViewController: OCKDailyPageViewController,
                                          prepare listViewController: OCKListViewController, for date: Date) {
        
        
        let talkPlantDataSeries = OCKDataSeriesConfiguration(
            taskID: "talkPlant",
            legendTitle: "Talks with plant",
            gradientStartColor: .systemBlue,
            gradientEndColor: .systemIndigo,
            markerSize: 10,
            eventAggregator: OCKEventAggregator.countOutcomeValues)

        let staringPlantDataSeries = OCKDataSeriesConfiguration(
            taskID: "staringPlant",
            legendTitle: "Plant staring",
            gradientStartColor: UIColor(named: "GreenSecondary")!,
            gradientEndColor: UIColor(named: "GreenPrimary")!,
            markerSize: 10,
            eventAggregator: OCKEventAggregator.countOutcomeValues)
        
        let insightsCard = OCKCartesianChartViewController(plotType: .bar, selectedDate: date,
                                                           configurations: [talkPlantDataSeries, staringPlantDataSeries],
                                                           storeManager: self.storeManager)
        insightsCard.chartView.headerView.titleLabel.text = "Plant Talking & Staring"
        insightsCard.chartView.headerView.detailLabel.text = "This Week"
        insightsCard.chartView.headerView.accessibilityLabel = "Plant Talking & Staring, This Week"
        listViewController.appendViewController(insightsCard, animated: false)
    }
    
}
