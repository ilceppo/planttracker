//
//  OCKTasks.swift
//  Plant Tracker
//
//  Created by Matteo Cioppa on 05/04/2020.
//  Copyright © 2020 Matteo Cioppa. All rights reserved.
//

import Foundation
import CareKit

extension OCKStore {

    // Adds tasks and contacts into the store
    func populateData() {
       
        // Create some date helper
        let thisMorning = Calendar.current.startOfDay(for: Date())
        let beforeBreakfast = Calendar.current.date(byAdding: .hour, value: 8, to: thisMorning)!
        let afterLunch = Calendar.current.date(byAdding: .hour, value: 14, to: thisMorning)!
        let beforeDinner = Calendar.current.date(byAdding: .hour, value: 18, to: thisMorning)!
        
        
        // Party hard with plant
        let partyHardSchedule = OCKSchedule.weeklyAtTime(weekday: 5,
                                                    hours: 22,
                                                    minutes: 0,
                                                    start: Date(),
                                                    end: nil,
                                                    targetValues: [],
                                                    text: "Before bedtime",
                                                    duration: .allDay)
                
        var partyHard = OCKTask(id: "partyHardPlant",
                             title: "Party hard with your plants",
                             carePlanID: nil,
                             schedule: partyHardSchedule)
        partyHard.impactsAdherence = true
        partyHard.instructions = "Turn up the volume and party like never before"
        
       
        // Talk with your plant
        let talkPlantSchedule = OCKSchedule(composing: [
            
            OCKScheduleElement(start: beforeBreakfast, end: nil,
                               interval: DateComponents(day: 1)),

            OCKScheduleElement(start: afterLunch, end: nil,
                               interval: DateComponents(day: 1)),
            
            OCKScheduleElement(start: beforeDinner, end: nil,
                                          interval: DateComponents(day: 1))
            
        ])

        var talkPlant = OCKTask(id: "talkPlant",
                                title: "Talk to your plant",
                                carePlanID: nil,
                                schedule: talkPlantSchedule)
        talkPlant.impactsAdherence = false
        talkPlant.instructions = "Tap the button below anytime you talk to your plant."

        
        // A plant is staring at me?
        let staringPlantSchedule = OCKSchedule(composing: [
            
                OCKScheduleElement(start: thisMorning,
                                   end: nil,
                                   interval: DateComponents(day: 1),
                                   text: "Anytime throughout the day",
                                   targetValues: [],
                                   duration: .allDay)
            
            ])

        var staringPlant = OCKTask(id: "staringPlant",
                                title: "Felt the plant staring at me",
                                carePlanID: nil,
                                schedule: staringPlantSchedule)
        staringPlant.impactsAdherence = false
        staringPlant.instructions = "Tap the button below anytime you feel observed and judged by your plant."
        
        // Irrigate your plant
        let irrigateSchedule = OCKSchedule.dailyAtTime(hour: 9, minutes: 0, start: Date(), end: nil, text: "Irrigate your plant")


        
        var irrigatePlant = OCKTask(id: "irrigatePlant",
                             title: "Irrigate your plant",
                             carePlanID: nil,
                             schedule: irrigateSchedule)
        irrigatePlant.impactsAdherence = true
        irrigatePlant.instructions = "Grab the purest water in the world and water your precious plant"
        
        
        
        
        addTasks([partyHard, talkPlant, staringPlant, irrigatePlant], callbackQueue: .main, completion: nil)
        
    }
}



