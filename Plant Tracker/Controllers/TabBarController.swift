//
//  TabBarController.swift
//  Plant Tracker
//
//  Created by Matteo Cioppa on 04/04/2020.
//  Copyright © 2020 Matteo Cioppa. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        let tasks = createTasksView()
        let charts = createChartView()
        
//        tasks.view.tintColor = UIColor.init(red: 0, green: 90, blue: 89, alpha: 1)
        charts.view.tintColor = UIColor(named: "GreenPrimary")
        tasks.view.tintColor = UIColor(named: "GreenPrimary")
        self.viewControllers = [
            tasks,
            charts
        ]
        
        tabBar.tintColor = UIColor(named: "GreenPrimary")
    }
    
    fileprivate func createTasksView() -> UINavigationController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = TasksController(storeManager: appDelegate.synchronizedStoreManager)
        
        viewController.title = "Tasks"
        viewController.tabBarItem.image = UIImage(systemName: "text.badge.checkmark")
        
        return UINavigationController(rootViewController: viewController)
    }
    
    fileprivate func createChartView() -> UINavigationController {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = ChartsController(storeManager: appDelegate.synchronizedStoreManager)
        
        viewController.title = "Charts"
        viewController.tabBarItem.image = UIImage(systemName: "chart.bar.fill")
        
        return UINavigationController(rootViewController: viewController)
    }
    
}
