//
//  TasksController.swift
//  Plant Tracker
//
//  Created by Matteo Cioppa on 04/04/2020.
//  Copyright © 2020 Matteo Cioppa. All rights reserved.
//

import UIKit
import CareKit

class TasksController: OCKDailyPageViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    
    
    override func dailyPageViewController(_ dailyPageViewController: OCKDailyPageViewController,
                                          prepare listViewController: OCKListViewController, for date: Date) {
        
        let identifiers = ["partyHardPlant", "talkPlant", "staringPlant", "irrigatePlant"]
        var query = OCKTaskQuery(for: date)
        query.ids = identifiers
        query.excludesTasksWithNoEvents = true
        
        storeManager.store.fetchAnyTasks(query: query, callbackQueue: .main) { result in
            switch result {
            case .failure(let error): print("Error: \(error)")
            case .success(let tasks):
                
//         Task Irrigate plant Daily
                
                if let irrigatePlantTask = tasks.first(where: { $0.id == "irrigatePlant" }) {
                    let irrigatePlantCard = OCKSimpleTaskViewController(task: irrigatePlantTask, eventQuery: .init(for: date),
                                                                        storeManager: self.storeManager)
                    listViewController.appendViewController(irrigatePlantCard, animated: false)
                }
  
//         Task Party Hard with plant Weekly
                
                if let partyHardPlantTask = tasks.first(where: { $0.id == "partyHardPlant" }) {
                                  let partyHardPlantCard = OCKSimpleTaskViewController(task: partyHardPlantTask, eventQuery: .init(for: date),
                                                                                      storeManager: self.storeManager)
                                  listViewController.appendViewController(partyHardPlantCard, animated: true)
                              }
//         Task Party Talk to plant Several times a day
                
                if let talkPlantTask = tasks.first(where: { $0.id == "talkPlant" }) {
                                  let talkPlantCard = OCKGridTaskViewController(task: talkPlantTask, eventQuery: .init(for: date),
                                                                                      storeManager: self.storeManager)
                                  listViewController.appendViewController(talkPlantCard, animated: false)
                              }
                
//         Task log plant staring at you
                
                if let staringPlantTask = tasks.first(where: { $0.id == "staringPlant" }) {
                    let staringPlantCard = OCKButtonLogTaskViewController(task: staringPlantTask, eventQuery: .init(for: date),
                                                                                        
                                                                                                                                          storeManager: self.storeManager)
                    listViewController.appendViewController(staringPlantCard, animated: false)
                }
            }
        }
    }
}


